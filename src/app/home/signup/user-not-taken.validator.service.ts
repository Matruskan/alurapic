import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { debounceTime, switchMap, map, first } from 'rxjs/operators';

import { SignUpService } from './signup.service';

@Injectable()
export class UserNotTakenValidatorService {

    constructor(private signUpService: SignUpService) {}

    checkUsernameTaken() {
        return (control: AbstractControl) => {
            return control
                .valueChanges
                .pipe(debounceTime(300))
                .pipe(switchMap(username =>
                        this.signUpService.checkUsernameTaken(username)
                ))
                .pipe(map(isTaken => isTaken ? { userNameTaken: true} : null ))
                .pipe(first());
        };
    }
}
